package com.example.talentomobile.coches.fragments;

import android.os.Bundle;
import android.widget.TextView;

import com.example.talentomobile.coches.R;
import com.example.talentomobile.coches.activities.CochesActivity;
import com.example.talentomobile.coches.base.BaseCochesFragment;
import com.example.talentomobile.coches.baseDatos.BaseDatos;
import com.example.talentomobile.coches.enums.AppCochesStates;
import com.tm.baseapp.enums.AppStates;
import com.tm.baseapp.utils.Argument;

/**
 * Created by TalentoMobile on 18/10/2016.
 */

public class DescripcionFragment extends BaseCochesFragment {
    private int idModeloCoche;
    private int numeroMarcaCoche;
    private TextView txtDescripcion;
    @Override
    public AppStates getStateId() {
        return AppCochesStates.DESCRIPCION;
    }
    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_descripcion;
    }


    @Override
    protected void initializeData() {
    }
    @Override
    public void refreshData(Argument args) {
        if(args==null)
            return;
        idModeloCoche = (int) args.get(CochesActivity.NUMERO_MODELO_COCHE);
        numeroMarcaCoche = (int)args.get(CochesActivity.NUMERO_MARCA_COCHE);
    }

    @Override
    protected void initializeUI() {
        txtDescripcion = (TextView) fragmentView.findViewById(R.id.textViewDescripcion);
        refreshData(getData());
        BaseDatos bd = new BaseDatos(getContext());
        txtDescripcion.setText("" + bd.getDescripcion(idModeloCoche, numeroMarcaCoche));
        bd.cerrarBD();
    }

    @Override
    protected void saveState() {
        super.saveState();
    }
}
