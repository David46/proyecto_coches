package com.example.talentomobile.coches.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.talentomobile.coches.R;
import com.example.talentomobile.coches.activities.CochesActivity;
import com.example.talentomobile.coches.adapters.AdaptadorPersonalizadoModelos;
import com.example.talentomobile.coches.adapters.Coche;
import com.example.talentomobile.coches.adapters.ViewPageAdapterCustom;
import com.example.talentomobile.coches.base.BaseCochesFragment;
import com.example.talentomobile.coches.baseDatos.BaseDatos;
import com.example.talentomobile.coches.enums.AppCochesStates;
import com.tm.baseapp.enums.AppStates;
import com.tm.baseapp.utils.Argument;

import java.util.ArrayList;

/**
 * Created by TalentoMobile on 18/10/2016.
 */

public class CarruselFragment extends BaseCochesFragment {

    private int numeroMarca;
    private ViewPager vp;
    private TextView txt;
    private ListView listaModelos;
    private ArrayList<String> arrayListaModelos;
    private ArrayList<Coche> arrayListCoches;
    private LinearLayout layoutNumPagina;
    private ArrayList<ImageView> arrayImageViews = new ArrayList<>();
    private HorizontalScrollView scroll;
    private static final int anchoIconosNumPagina = 70;
    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_carrusel;
    }

    @Override
    public AppStates getStateId() {
        return AppCochesStates.CARRUSEL;
    }


    @Override
    public void refreshData(Argument args) {
        if(args!=null)
            numeroMarca = (int) args.get(CochesActivity.NUMERO_MARCA_COCHE);
    }

    @Override
    protected void initializeData() {

    }

    @Override
    protected void initializeUI() {

        listaModelos = (ListView) fragmentView.findViewById(R.id.listaModelos);
        layoutNumPagina = (LinearLayout) fragmentView.findViewById(R.id.layoutNumPagina);
        refreshData(getData());
        onClickLista();
        scroll = (HorizontalScrollView) fragmentView.findViewById(R.id.scrollViewNumPagina);
        crearCarrusel();

    }

    private void crearCarrusel() {
        cargarCochesFromDatabase();
        vp = (ViewPager) fragmentView.findViewById(R.id.id_carruselViewPager);
        vp.setAdapter(new ViewPageAdapterCustom(getContext(),arrayListCoches));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(anchoIconosNumPagina, anchoIconosNumPagina); // para las imagenes 100*100
        arrayImageViews = new ArrayList<>();
        for (int i = 0; i <arrayListCoches.size() ; i++) {
             arrayImageViews.add(new ImageView(managerActivity));
             arrayImageViews.get(i).setImageResource(R.drawable.num_pagina);
             arrayImageViews.get(i).setLayoutParams(layoutParams);
             arrayImageViews.get(i).setId(i);
            layoutNumPagina.addView( arrayImageViews.get(i));
            arrayImageViews.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(view instanceof ImageView){
                        ImageView iv = (ImageView) view;
                        refrescar(iv.getId());
                    }
                }
            });
        }
        refrescar(numeroMarca);

        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                refrescar(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void onClickLista() {
        listaModelos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Argument args = new Argument();
                args.put(CochesActivity.NUMERO_MODELO_COCHE,i);
                args.put(CochesActivity.NUMERO_MARCA_COCHE,numeroMarca);
                changeToNextState(args);
            }
        });
    }


    private  void cargarCochesFromDatabase(){
        BaseDatos db = new BaseDatos(getContext());
        arrayListCoches = db.getMarcas();
        db.cerrarBD();

    }
    private void refrescar (int position){
        numeroMarca=position;
        BaseDatos bd = new BaseDatos(getContext());
        arrayListaModelos = bd.getModelos(numeroMarca);
        bd.cerrarBD();
        listaModelos.setAdapter(new AdaptadorPersonalizadoModelos(getContext(),arrayListaModelos));
        for(ImageView iv :arrayImageViews){
            iv.setImageResource(R.drawable.num_pagina);
        }
        arrayImageViews.get(position).setImageResource(R.drawable.num_pagina_selecionada);
        vp.setCurrentItem(numeroMarca);
    }
    private int calcularAncho(){
        DisplayMetrics displaymetrics = new DisplayMetrics();
        managerActivity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.widthPixels;
    }

    @Override
    protected void saveState() {
        super.saveState();
    }
}
