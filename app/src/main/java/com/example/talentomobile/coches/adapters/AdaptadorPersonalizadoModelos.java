package com.example.talentomobile.coches.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.talentomobile.coches.R;

import java.util.ArrayList;

/**
 * Created by TalentoMobile on 21/10/2016.
 */

public class AdaptadorPersonalizadoModelos extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<String> arrayModelos;

    public AdaptadorPersonalizadoModelos(Context context, ArrayList<String> arrayModelos) {
        this.context = context;
        this.arrayModelos = arrayModelos;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return arrayModelos.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    public static class ViewHolder{
        TextView txtModelo;
        ImageView imagenMarca;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder myHolder;
        if(view == null){
            view = inflater.inflate(R.layout.fila_lista_modelos,parent,false);
            myHolder = new ViewHolder();
            myHolder.txtModelo = (TextView) view.findViewById(R.id.txtModelo);
            view.setTag(myHolder);
        }else{
            myHolder = (ViewHolder) view.getTag();
        }
        myHolder.txtModelo.setText(""+arrayModelos.get(position));
        return view;
    }
}
