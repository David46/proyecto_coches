package com.example.talentomobile.coches.base;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.content.res.ResourcesCompat;

import com.example.talentomobile.coches.BaseCochesApplication;
import com.example.talentomobile.coches.R;
import com.example.talentomobile.coches.views.CustomActionBar;
import com.tm.baseapp.activity.BaseActivity;


/**
 * Created by TalentoMobile on 18/10/2016.
 */

public abstract class BaseCochesActivity extends BaseActivity{
    protected CustomActionBar actionBar;
    protected BaseCochesApplication app;
    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        app= (BaseCochesApplication) getApp();
    }

    @Override
    protected int getActivityLayout() {return R.layout.activity_base_native;}
    @Override
    protected int getFragmentContainer() {
        return R.id.fragment_container;
    }

    @Override
    protected void initializeActionBar() {
        actionBar = (CustomActionBar) findViewById(R.id.container_custom_action_bar);
        actionBar.setTituloColor(ResourcesCompat.getColor(getResources(), R.color.colorWhite, null));
    }
}
