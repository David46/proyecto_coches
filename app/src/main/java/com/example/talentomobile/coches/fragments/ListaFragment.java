package com.example.talentomobile.coches.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.talentomobile.coches.R;
import com.example.talentomobile.coches.activities.CochesActivity;
import com.example.talentomobile.coches.adapters.AdaptadorPersonalizadoMarcas;
import com.example.talentomobile.coches.adapters.Coche;
import com.example.talentomobile.coches.base.BaseCochesFragment;
import com.example.talentomobile.coches.baseDatos.BaseDatos;
import com.example.talentomobile.coches.enums.AppCochesStates;
import com.tm.baseapp.enums.AppStates;
import com.tm.baseapp.utils.Argument;

import java.util.ArrayList;


/**
 * Created by TalentoMobile on 18/10/2016.
 */

public class ListaFragment extends BaseCochesFragment {
    private ListView miLista;
    private ArrayList<Coche> arrayCoches = new ArrayList<>();

    @Override
    public AppStates getStateId() {
        return AppCochesStates.LISTA; // estado
    }
    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_lista; // layout
    }

    @Override
    protected void initializeData() {

    }
    @Override
    protected void initializeUI() { // inicializar componentes y listeners
        miLista = (ListView) fragmentView.findViewById(R.id.miListaCoches);
        cargarCochesFromDatabase();
        miLista.setAdapter(new AdaptadorPersonalizadoMarcas(getActivity(),arrayCoches));
        onClickLista();
    }

    @Override
    protected void saveState() {

    }
    private  void cargarCochesFromDatabase(){

        BaseDatos db = new BaseDatos(getContext());
        arrayCoches = db.getMarcas();
        db.cerrarBD();

    }

    private void onClickLista() {
        miLista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(managerActivity, ""+position, Toast.LENGTH_SHORT).show();
                Argument arg = new Argument();
                arg.put(CochesActivity.NUMERO_MARCA_COCHE,arrayCoches.get(position).getId_marca());
                changeToNextState(arg);
            }
        });
    }

}
