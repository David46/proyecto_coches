package com.example.talentomobile.coches.activities;

import android.content.Intent;

import com.example.talentomobile.coches.base.BaseCochesActivity;
import com.example.talentomobile.coches.enums.AppCochesActivities;
import com.example.talentomobile.coches.enums.AppCochesStates;
import com.example.talentomobile.coches.fragments.SplashFragment;
import com.tm.baseapp.enums.AppActivity;
import com.tm.baseapp.enums.AppStates;
import com.tm.baseapp.fragment.BaseFragment;
import com.tm.baseapp.utils.Argument;


/**
 * Created by TalentoMobile on 25/10/2016.
 */

public class SplashActivity extends BaseCochesActivity{
    @Override
    public AppActivity getActivityId() {
        return AppCochesActivities.SPLASH_ACTIVITY;
    }

    @Override
    protected AppStates getNextState(AppStates currentState, Argument args) {
        return AppCochesStates.LISTA;
    }
    @Override
    protected AppStates getPreviousState(AppStates state) {
        return AppCochesStates.NOTHING_TODO;
    }

    @Override
    protected void switchState(AppStates state, Argument args) {
        BaseFragment fragment = null;
        Intent intent = null;
        switch ((AppCochesStates)state){
            case LISTA:
                intent = new Intent(this,CochesActivity.class);
                //intent = new Intent(AppCochesActivities.COCHES_ACTIVITY.getName());
                break;
            case SPLASH:
            default:
                actionBar.setTituloBar("Proyecto Coches");
                fragment = new SplashFragment();
        }
        if(fragment!=null){
            if(args!=null)
            fragment.setData(args);
            navigateTo(fragment);

        }
        if(intent!=null){
            navigateTo(intent,true);
        }

    }

    @Override
    protected void setContentActionBar() {

    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();

    }

    @Override
    public void initDatamanager() {

    }

    @Override
    protected void initializeNavigationDrawer() {

    }

    @Override
    protected void initializeUI() {

    }

    @Override
    protected void initializeData() {

    }
    @Override
    protected void refreshData() {

    }


}
