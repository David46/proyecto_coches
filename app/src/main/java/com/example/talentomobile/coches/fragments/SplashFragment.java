package com.example.talentomobile.coches.fragments;

import com.example.talentomobile.coches.R;
import com.example.talentomobile.coches.base.BaseCochesFragment;
import com.example.talentomobile.coches.enums.AppCochesStates;
import com.tm.baseapp.enums.AppStates;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by TalentoMobile on 25/10/2016.
 */

public class SplashFragment extends BaseCochesFragment {

    private final static long SPLASH_DELAY=2500;

    @Override
    protected void initializeData() {

    }
    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_splash;
    }

    @Override
    public AppStates getStateId() {
        return AppCochesStates.SPLASH;
    }

    @Override
    protected void initializeUI() {
        Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                changeToNextState(null);
            }
        },SPLASH_DELAY);

    }


}
