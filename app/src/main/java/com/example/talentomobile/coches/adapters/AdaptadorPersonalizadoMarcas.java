package com.example.talentomobile.coches.adapters;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.talentomobile.coches.R;

import java.util.ArrayList;

/**
 * Created by TalentoMobile on 19/10/2016.
 */

public class AdaptadorPersonalizadoMarcas extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Coche> arrayCoches;

    public AdaptadorPersonalizadoMarcas(Context context, ArrayList<Coche> arrayCoches) {
        this.context = context;
        this.arrayCoches = arrayCoches;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return arrayCoches.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    public static class ViewHolder{
        TextView txtMarcaCoche;
        ImageView imagenMarca;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder myHolder;
        if(view == null){
            view = inflater.inflate(R.layout.fila_lista_coches,parent,false);
            myHolder = new ViewHolder();
            myHolder.imagenMarca = (ImageView) view.findViewById(R.id.imagen_marca);
            myHolder.txtMarcaCoche = (TextView) view.findViewById(R.id.txt_marca);
            view.setTag(myHolder);
        }else{
           myHolder = (ViewHolder) view.getTag();
        }
        int res = context.getResources().getIdentifier(arrayCoches.get(position).getMarca().toLowerCase(), "drawable", context.getPackageName());
        myHolder.imagenMarca.setImageResource(res);
        myHolder.txtMarcaCoche.setText(""+arrayCoches.get(position).getMarca());
        return view;
    }
}
