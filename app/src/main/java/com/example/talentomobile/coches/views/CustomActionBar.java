package com.example.talentomobile.coches.views;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.talentomobile.coches.R;

public class CustomActionBar extends FrameLayout {

    private TextView txtTituloBar;
    private ImageView imgLeftBar;
    private ImageView imgRightBar;


    public CustomActionBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.custom_action_bar,this);
        txtTituloBar = (TextView) findViewById(R.id.title_action_bar);
        imgLeftBar = (ImageView) findViewById(R.id.img_action_bar_left);
        imgRightBar = (ImageView) findViewById(R.id.img_actionBar_right);
    }
    public void setTituloBar(String titulo){
        if(txtTituloBar!=null)
            txtTituloBar.setText(""+titulo);
    }
    public void setImgLeftBar(Drawable res){
        if(imgLeftBar!=null)
            imgLeftBar.setBackground(res);
    }

    public void setImgRightBar(int res) {
        if(imgLeftBar!=null)
            imgRightBar.setImageResource(res);
    }
    public void setOnClickImgLeftBar(OnClickListener listener){
        if(imgLeftBar!=null)
            imgLeftBar.setOnClickListener(listener);
    }
    public void setOnClickImgRightBar(OnClickListener listener){
        if(imgRightBar!=null)
            imgRightBar.setOnClickListener(listener);
    }
    public void setTituloColor(int color){
        txtTituloBar.setTextColor(color);
    }


}
