package com.example.talentomobile.coches.baseDatos;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.example.talentomobile.coches.R;
import com.example.talentomobile.coches.adapters.Coche;

import java.util.ArrayList;

/**
 * Created by TalentoMobile on 21/10/2016.
 */

public class BaseDatos {
        private SQLiteDatabase db;
        private Context ctx;
        private final String [] arrayMarcas={"Alfa","Audi","Bmw","Chevrolet","Citroen","Ferrari","Fiat","Ford","Honda","Hyundai"
                ,"Jaguar","Jeep","Kia","Lancia","Lexus","Mazda","Mercedes","Mg","Mini","Mitsubishi","Nissan"
                ,"opel","peugeot","porsche","renault","seat","skoda","subaru","tata","toyota","volkswagen","volvo"};
        private final String [][] arrayModelos={{"Giulietta,Mitto,4C,Giulia"},{"A1,A3,A4,A5,A6"},{"Serie 1,Serie 2,Serie 3,Serie 4,Serie 5","X1","X5"}
                ,{"Corvette,Camaro,Spin"},{"C1,C3,C4,C5,Picasso"},{"California,F12,Spider,F350"},{"500,Panda,Punto,Doblo"},{"Focus,Focus RS"},{"Civic,Accord"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"},{"A1,A3,A4,A5,A6"}
        };
        private final String [][] arrayDescripcion={
                {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"},
                {"A1 250cv 20.000€","A3 300cv 30.000€","A4 340 cv 40.000€","A5 500 cv 50.000€","A6 500 cv 60.000€"},
                {"Serie 1 250cv","Serie 2 300cv","Serie 3 340 cv","Serie 4 500 cv"}, {"Corvette 250cv","Camaro 300cv","Spin 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"}, {"Giulietta 250cv","Mitto 300cv","4C 340 cv","Giulia 500 cv"},
        };

        public BaseDatos(Context ctx) {
            this.ctx = ctx;
            db = ctx.openOrCreateDatabase("CochesBD", Context.MODE_PRIVATE, null);
            db.execSQL("CREATE TABLE IF NOT EXISTS marcasCoches" +
                    "( Id INTEGER PRIMARY KEY, NombreMarca VARCHAR NOT NULL);");
            db.execSQL("CREATE TABLE IF NOT EXISTS modelosMarca" +
                    "( Id INTEGER PRIMARY KEY AUTOINCREMENT, NombreModelo VARCHAR NOT NULL, IdMarca INT NOT NULL);");
            db.execSQL("CREATE TABLE IF NOT EXISTS descripcionModelos" +
                    "( Id INTEGER PRIMARY KEY AUTOINCREMENT, DescripcionModelo VARCHAR NOT NULL, IdModelo INT NOT NULL);");
            insertarDatos();

            ///   0 A1 1, 1 A2 1
        }

        private void insertarDatos() {


            try{
                for (int i = 0; i <arrayMarcas.length ; i++) {
                    db.execSQL("INSERT INTO marcasCoches VALUES('"+i+"','"+arrayMarcas[i]+"')");
                }
                for (int i = 0; i <arrayModelos.length ; i++) {
                    for (int j = 0; j <arrayModelos[i].length ; j++) {
                        String [] arrayModelos2 = arrayModelos[i][j].split(",");
                        // String tring [][] arrayModelos={{"Giulietta,Mitto,4C,Giulia"},{"A1,A3,A4,A5,A6"}}
                        for (String s:arrayModelos2) {
                            db.execSQL("INSERT INTO modelosMarca VALUES(null,'"+s+"','"+i+"')");
                        }
                    }
                }
                for (int i = 0; i <arrayDescripcion.length ; i++) {
                    for (int j = 0; j <arrayDescripcion[i].length ; j++) {
                        String [] arrayDescripcion2 = arrayDescripcion[i][j].split(",");
                        for (String s:arrayDescripcion2) {
                            System.out.println("here "+s);
                            db.execSQL("INSERT INTO descripcionModelos VALUES(null,'"+s+"','"+i+"')");
                        }
                    }
                }
            }catch (Exception e){
                return; // ya estaban añadidas
            }
                                                                                           }
        public SQLiteDatabase getDatabase(){
            return db;
        }

        public ArrayList<Coche> getMarcas(){
            ArrayList<Coche> arrayMarcas = new ArrayList<>();
            Cursor c = db.rawQuery("SELECT * FROM marcasCoches", null);
            if(c.getCount()==0){
                return null;
            }
               while (c.moveToNext()){
                   arrayMarcas.add(new Coche(Integer.parseInt(c.getString(0)),c.getString(1),null));  // 2,Audi
               }
            return arrayMarcas;
        }
        public ArrayList<String> getModelos(int numeroMarca){
            ArrayList<String> arrayModelos = new ArrayList<>();
            Cursor c = db.rawQuery("SELECT * FROM modelosMarca WHERE IdMarca='"+numeroMarca+"'", null);
             if(c.getCount()==0){
                arrayModelos.add("No hay registros");
                 return arrayModelos;
             }
            while(c.moveToNext()) {
                arrayModelos.add(""+c.getString(1));
            }
           return arrayModelos;
        }
        public String getDescripcion(int numeroModelo, int numeroMarcaCoche){
            ArrayList<String> arrayDescripcion = new ArrayList<>();
            String descripcion;
            Cursor c = db.rawQuery("SELECT * FROM descripcionModelos WHERE IdModelo='"+numeroMarcaCoche+"'", null);
            if(c.getCount()==0){
                descripcion = "No hay registros";
                return descripcion;
            }
            while(c.moveToNext()) {
                arrayDescripcion.add(""+c.getString(1));
            }
            descripcion = arrayDescripcion.get(numeroModelo);
            return descripcion;
        }
        public void cerrarBD(){
            db.close();
        }

    }

