package com.example.talentomobile.coches.enums;

import com.tm.baseapp.enums.AppStates;

/**
 * Created by TalentoMobile on 20/10/2016.
 */

public enum AppCochesStates implements AppStates {
    INITIAL,
    LOGOUT_NAVIGATION_REQUEST,
    MINIMIZE_NAVIGATION_REQUEST,
    BACK_ACTIVITY_NAVIGATION_REQUEST,
    NOTHING_TODO,
    MESSAGE,
    LISTA,
    CARRUSEL,
    DESCRIPCION,
    SPLASH,
}
