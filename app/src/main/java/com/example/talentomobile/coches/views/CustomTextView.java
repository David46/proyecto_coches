package com.example.talentomobile.coches.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

import com.example.talentomobile.coches.R;
import com.example.talentomobile.coches.utils.Utils;

/**
 * Created by TalentoMobile on 26/10/2016.
 */

public class CustomTextView extends TextView {
    public CustomTextView(Context context) {
        super(context);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context,attrs);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont(context,attrs);

    }
    private void setFont(Context context, AttributeSet attrs) {

        TypedArray styleAttributes = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        int font;
        if(styleAttributes!=null){
            font = styleAttributes.getInteger(R.styleable.CustomTextView_font_style, Utils.NOTARIZED);
            Utils.setCustomFont(this, font);
            styleAttributes.recycle();
        }
    }
}
