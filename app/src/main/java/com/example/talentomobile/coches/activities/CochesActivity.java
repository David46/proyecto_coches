package com.example.talentomobile.coches.activities;

import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.widget.Toast;

import com.example.talentomobile.coches.R;
import com.example.talentomobile.coches.base.BaseCochesActivity;
import com.example.talentomobile.coches.enums.AppCochesActivities;
import com.example.talentomobile.coches.enums.AppCochesStates;
import com.example.talentomobile.coches.fragments.CarruselFragment;
import com.example.talentomobile.coches.fragments.DescripcionFragment;
import com.example.talentomobile.coches.fragments.ListaFragment;
import com.tm.baseapp.enums.AppActivity;
import com.tm.baseapp.enums.AppStates;
import com.tm.baseapp.fragment.BaseFragment;
import com.tm.baseapp.utils.Argument;


public class CochesActivity extends BaseCochesActivity {
    public static final String NUMERO_MARCA_COCHE="marcaCoche"; // tag que utilizo para pasar de un fragment a otro
    public static final String NUMERO_MODELO_COCHE="idModeloCoche";
    // dado un estado devuelvo el siguiente
    @Override
    protected AppStates getNextState(AppStates currentState, Argument args) {
        switch ((AppCochesStates)currentState){
            case LISTA:
                return AppCochesStates.CARRUSEL;
            case CARRUSEL:
                return AppCochesStates.DESCRIPCION;
        }
        return AppCochesStates.LISTA;
    }
    // dado un estado volver al anterior
    @Override
    protected AppStates getPreviousState(AppStates state) {
        switch ((AppCochesStates)state){
            case CARRUSEL:
                return AppCochesStates.LISTA;
            case DESCRIPCION:
                return AppCochesStates.CARRUSEL;
            case LISTA:
                return AppCochesStates.MINIMIZE_NAVIGATION_REQUEST;

        }
          return AppCochesStates.MINIMIZE_NAVIGATION_REQUEST;
    }

    // dado un estado lanzo el fragment o la actividad que corresponda con los arguments
    @Override
    protected void switchState(AppStates state, Argument args) {
        BaseFragment fragment;
        Intent intent=null;
        switch ((AppCochesStates)state){
            case LISTA:
            default:
                actionBar.setTituloBar("Marcas");
                fragment = new ListaFragment();
                break;
            case CARRUSEL:
                actionBar.setTituloBar("Modelos");
                fragment = new CarruselFragment();
                break;
            case DESCRIPCION:
                actionBar.setTituloBar("Descripción");
                fragment = new DescripcionFragment();
                break;
        }
        if(fragment!=null){
            if(args!=null)
                fragment.setData(args);
            navigateTo(fragment);
        }
        if(intent!=null){
            if(args!=null)
            navigateTo(intent,true);
        }
    }

    @Override
    protected void setContentActionBar() {

    }

    @Override
    protected void initializeActionBar() {
        super.initializeActionBar();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            actionBar.setImgLeftBar(getResources().getDrawable(R.drawable.img_left_icon,null));
        }else{
            actionBar.setImgLeftBar(getResources().getDrawable(R.drawable.img_left_icon));
        }
        actionBar.setOnClickImgLeftBar(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        actionBar.setImgRightBar(R.drawable.logo_splash);
        actionBar.setOnClickImgRightBar(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getBaseContext(), "Burger King", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void initDatamanager() {

    }

    @Override
    protected void initializeNavigationDrawer() {

    }

    @Override
    protected void initializeUI() {

    }

    @Override
    protected void initializeData() {

    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_base_native;
    }

    @Override
    public AppActivity getActivityId() {
        return AppCochesActivities.COCHES_ACTIVITY;
    }

    @Override
    protected void refreshData() {
    }


    @Override
    protected int getFragmentContainer() {
        return R.id.fragment_container;
    }

}