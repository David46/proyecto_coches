package com.example.talentomobile.coches.adapters;

/**
 * Created by TalentoMobile on 19/10/2016.
 */

public class Coche {

    private int id_marca;
    private String marca;
    private String [] modelos;

    public Coche(){}
    public Coche(int id_marca,String marca, String [] modelos) {
        this.id_marca = id_marca;
        this.marca = marca;
        this.modelos = modelos;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getId_marca() {
        return id_marca;
    }

    public void setId_marca(int id_marca) {
        this.id_marca = id_marca;
    }


}
