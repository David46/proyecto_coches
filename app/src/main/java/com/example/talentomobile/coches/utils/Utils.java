package com.example.talentomobile.coches.utils;

import android.widget.TextView;

import com.example.talentomobile.coches.BaseCochesApplication;

/**
 * Created by TalentoMobile on 26/10/2016.
 */

public class Utils {

    public static final int APPLEBERRY =0;
    public static final int NOTARIZED =1;
    public static final int NOTARIZEDOBLIQUE =2;
    public static final int SNACKERCOMIC =3;

    public static void setCustomFont(TextView view, int font){
        switch (font){
            case APPLEBERRY:
                view.setTypeface(BaseCochesApplication.Fonts.APPLEBERRY);
                break;
            case NOTARIZED:
                view.setTypeface(BaseCochesApplication.Fonts.NOTARIZED);
                break;
            case NOTARIZEDOBLIQUE:
                view.setTypeface(BaseCochesApplication.Fonts.NOTARIZEDOBLIQUE);
                break;
            case SNACKERCOMIC:
                view.setTypeface(BaseCochesApplication.Fonts.SNACKERCOMIC);
                break;
        }
    }
}
