package com.example.talentomobile.coches.enums;

import com.tm.baseapp.enums.AppActivity;

/**
 * Created by TalentoMobile on 25/10/2016.
 */

public enum AppCochesActivities implements AppActivity{
    COCHES_ACTIVITY("coches.activities.CochesActivity"),
    SPLASH_ACTIVITY("coches.activities.SplashActivity");

    String name;
    AppCochesActivities(String name) {
        this.name = "com.example.talentomobile."+name;
    }

    @Override
    public String getName() {
        return name;
    }
}
