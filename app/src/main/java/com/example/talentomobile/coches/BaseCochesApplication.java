package com.example.talentomobile.coches;


import android.graphics.Typeface;

import com.example.talentomobile.coches.enums.AppCochesStates;
import com.tm.baseapp.application.BaseApplication;
import com.tm.baseapp.enums.AppStates;

/**
 * Created by TalentoMobile on 20/10/2016.
 */

public class BaseCochesApplication extends BaseApplication{
    @Override
    public void onCreate() {
        super.onCreate();
        initTypefaces();
    }

    @Override
    public AppStates getUninitializedState() {
        return AppCochesStates.INITIAL;
    }

    @Override
    public AppStates getLogoutNavigationRequestState() {
        return AppCochesStates.LOGOUT_NAVIGATION_REQUEST;
    }

    @Override
    public AppStates getMinimizeNavigationRequestState() {
        return AppCochesStates.MINIMIZE_NAVIGATION_REQUEST;
    }

    @Override
    public AppStates getBackActivityNavigationRequestState() {
        return AppCochesStates.BACK_ACTIVITY_NAVIGATION_REQUEST;
    }

    @Override
    public AppStates getNothingToDoNavigationRequestState() {
        return AppCochesStates.NOTHING_TODO;
    }

    @Override
    public AppStates getMessageState() {
        return AppCochesStates.MESSAGE;
    }


    public static class Fonts {

        public static Typeface APPLEBERRY;
        public static Typeface NOTARIZED;
        public static Typeface NOTARIZEDOBLIQUE;
        public static Typeface SNACKERCOMIC;

    }

    public void initTypefaces() {
        Fonts.APPLEBERRY = Typeface.createFromAsset(getAssets(), "fonts/Appleberry.ttf");
        Fonts.NOTARIZED = Typeface.createFromAsset(getAssets(), "fonts/Notarized.ttf");
        Fonts.NOTARIZEDOBLIQUE = Typeface.createFromAsset(getAssets(), "fonts/NotarizedOblique.ttf");
        Fonts.SNACKERCOMIC = Typeface.createFromAsset(getAssets(), "fonts/SnackerComic.ttf");
    }
}
