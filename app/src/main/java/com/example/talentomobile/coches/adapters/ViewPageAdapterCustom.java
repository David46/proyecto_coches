package com.example.talentomobile.coches.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.talentomobile.coches.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by TalentoMobile on 24/10/2016.
 */

public class ViewPageAdapterCustom extends PagerAdapter {
    private Context context;
    private ArrayList<Coche> arrayCoches;
    private LayoutInflater inflater;

    public ViewPageAdapterCustom(Context context, ArrayList<Coche> arrayCoches) {
        this.context = context;
        this.arrayCoches = arrayCoches;
        this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return arrayCoches.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
       View itemView = inflater.inflate(R.layout.fila_view_pager,container,false);
        TextView txtNombreModelo = (TextView) itemView.findViewById(R.id.txtModelo);
        ImageView imgModelo = (ImageView) itemView.findViewById(R.id.imgModelo);
        txtNombreModelo.setText(arrayCoches.get(position).getMarca()+" ");
        int res = context.getResources().getIdentifier(arrayCoches.get(position).getMarca().toLowerCase(), "drawable", context.getPackageName());
        imgModelo.setImageResource(res);
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
